===================================================================================================
Simulation framework
===================================================================================================

This page documents the Ulula hydro solver and the implemented algorithms.

---------------------------------------------------------------------------------------------------
Overview
---------------------------------------------------------------------------------------------------

The Ulula simulation framework is basically implemented within a single class, called
:class:`~ulula.simulation.Simulation`. The :class:`~ulula.simulation.HydroScheme` class contains 
information about the chosen algorithm. The user does not need to interact directly with the 
:class:`~ulula.simulation.Simulation` class because the simulation workflow is already implemented 
in the :func:`ulula.run.run` runtime function (see :doc:`run`). If this workflow is not general 
enough, however, the user can run a simulation by implementing and/or modifying the following 
steps:

* Create a :class:`~ulula.simulation.HydroScheme` object that contains all user-defined 
  algorithmic settings
* Create the :class:`~ulula.simulation.Simulation` (passing the HydroScheme to the constructor)
* Set up the domain and variable arrays with the :func:`~ulula.simulation.Simulation.setDomain` 
  function, which sets
  the size of the domain in pixels and physical coordinates, as well as the boundary conditions
* Set the fluid properties (such as the adiabatic index) using the 
  :func:`~ulula.simulation.Simulation.setFluidProperties` function
* Set the initial conditions in primitive variables (into the simulation object's
  ``V`` array); this operation is implemented in each problem setup class (see :doc:`setups`).
  The primitive variable fields are listed below.
* Execute the :func:`~ulula.simulation.Simulation.timestep` function until the simulation is 
  finished; create plots as desired

The options for hydro algorithms are described in the :class:`~ulula.simulation.HydroScheme` 
class below. The documentation of :class:`~ulula.simulation.Simulation` contains further details 
about the inner workings of the Ulula hydro solver. 

Ulula can save the current state of a simulation to an hdf5 file (see the 
:func:`~ulula.simulation.Simulation.save` function). The 
:func:`~ulula.simulation.load` function loads such a file and recovers a Simulation object 
identical to the one saved. The simulation can then be plotted or continued, meaning that the 
files serve as both snapshot and restart files.

---------------------------------------------------------------------------------------------------
Fluid quantities
---------------------------------------------------------------------------------------------------

The following identifiers are used for fluid variables throughout the code:

============  ======
Abbreviation  Quantity
============  ======
Primitive variables
--------------------
``DN``        Density
``VX``        X-velocity
``VY``        Y-velocity
``PR``        Pressure
Conserved variables
--------------------
``MS``        Density (here called mass for consistency)
``MX``        X-momentum
``MY``        Y-momentum
``ET``        Total energy (thermal + kinetic + potentials)
============  ======

These abbreviations are used to indicate quantities when plotting, although a few additional 
plottable quantities are defined (see :doc:`plots`). Similarly, the abbreviations are used when
setting fluid variables in a :class:`~ulula.simulation.Simulation` object (see the ``q_prim`` and
``q_cons`` fields).

---------------------------------------------------------------------------------------------------
Hydro schemes
---------------------------------------------------------------------------------------------------

The following tables list the algorithmic choices implemented in Ulula. Broadly speaking, Ulula 
uses a Godunov solver with either piecewise-constant or piecewise-linear state reconstruction and 
a either Euler or 2nd-order time integration (MUSCL-Hancock). 

.. list-table:: Spatial reconstruction schemes
   :widths: 20 80
   :header-rows: 1

   * - Identifier
     - Description
   * - ``const``
     - Piecewise-constant; the scheme is 1st-order in space because the state within each cell is 
       not interpolated. This hydro scheme tends to be highly diffusive and is impelemented for
       test purposes.
   * - ``linear``
     - Piecewise-linear; linear interpolation within each cell, the scheme is 2nd-order in space. 
       The slope limiter decides how aggressive the reconstruction is in terms of accuracy vs. 
       stability.

.. list-table:: Slope limiters
   :widths: 20 80
   :header-rows: 1

   * - Identifier
     - Description
   * - ``none``
     - No limiter; results in a highly unstable scheme because the interpolation can lead to
       negative values and other artifacts. Implemented for demonstration purposes only
   * - ``minmod``
     - Minimum modulus; the most conservative limiter, takes the shallower of the left and right 
       slopes
   * - ``vanleer``
     - The van Leer limiter; an intermediate between the minmod and mc limiters
   * - ``mc``
     - Monotonized central; the most aggressive limiter, takes the central slope unless the slopes
       are very different

.. list-table:: Riemann solvers
   :widths: 20 80
   :header-rows: 1

   * - Identifier
     - Description
   * - ``hll``
     - The Harten-Lax-van Leer (HLL) Riemann solver; this simple algorithm considers only the 
       fastest waves to the left and right and constructs an intermediate state, ignoring contact
       discontinuities.

.. list-table:: Time integration schemes
   :widths: 20 80
   :header-rows: 1

   * - Identifier
     - Description
   * - ``euler``
     - First-order time integration; the fluid state is advanced by a full timestep without any
       attempt to time-average the Godunov fluxes.
   * - ``hancock``
     - Combined with linear reconstruction, this choice gives the MUSCL-Hancock scheme, where the
       states at the left and right cell edges are advanced by a half timestep. The change in time
       is computed to linear order from the primitive-variable form of the Euler equations, applied
       to the left and right states separately. 
   * - ``hancock_cons``
     - The same as ``hancock``, but using the conserved formulation where the fluxes corresponding
       to the left and right cell-edge states are computed and differenced to get an evolution in 
       the fluid state. This formulation should be identical to the primitive formulation up to 
       numerical errors, which provides a check of the algorithms. The conservative version is 
       slower due to a number of primitive-conserved conversions; thus, the primitive version is
       preferred.

The purpose of Ulula is to experiment with hydro algorithms, including their failure. Thus, the
code allows sub-optimal (aka crazy) combinations of the parameters listed above. For
example, it allows setting no limiter or combining piecewise-constant states with a Hancock-style
time integration, which results in an unstable scheme. The user's algorithmic choices are 
contained in the HydroScheme class:

.. autoclass:: ulula.simulation.HydroScheme
    :members:
    
---------------------------------------------------------------------------------------------------
Simulation class
---------------------------------------------------------------------------------------------------

.. autoclass:: ulula.simulation.Simulation
    :members:

---------------------------------------------------------------------------------------------------
Input/Output
---------------------------------------------------------------------------------------------------

Files are saved using the :func:`~ulula.simulation.Simulation.save` function in a Simulation 
object. The file format is relatively self-explanatory. The attributes of the ``hydro_scheme`` 
group correspond to the parameters of the :class:`~ulula.simulation.HydroScheme` object. The
attributes of the ``domain``, ``physics``, and ``run`` groups have the same meaning as in the 
:class:`~ulula.simulation.Simulation` object. The ``code`` group contains the version of Ulula that
the filetype corresponds to. 

The ``grid`` group contains the 2D grid data for the given simulation in code units. The field 
names correspond to the abbreviations listed in the "Fluid quantities" section above. Only the 
physical domain is included (without ghost cells).

A file can be loaded into a new simulation object with the following function. It checks the 
file version against the current version of Ulula; if the file version is too old for the file to
be compatible, the loading process is aborted.

.. autofunction:: ulula.simulation.load
   