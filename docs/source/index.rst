===================================================================================================
Ulula Documentation
===================================================================================================

Ulula is an ultra-lightweight 2D hydrodynamics code for teaching and experimentation. Written in 
Python, it is designed to be compact and easy to comprehend while not compromising on performance. 
Ulula solves the Eulerian fluid equations on a grid using the Godunov method. The name is a play on 
words: Euler’s name contains the German word for owl, Eule. Ulula means owl in Latin, the language 
in which Euler wrote most of his papers.

The main motivation behind Ulula is to create a laboratory for important algorithms used in 
numerical hydrodynamics. In full-blown production codes, these algorithms tend to be obscured by 
layers of technical code for parallelization, adaptive meshes, and so on. The brevity of the 
Ulula solver makes it easy to identify, switch, and modify the main components such as 
reconstruction, slope limiter, Riemann solver, and timestepping. 

Given its simplicity, Ulula is not meant to compete with more complex python codes such as 
`Pyro <https://python-hydro.github.io/pyro2/>`_. 
Similarly, Ulula is not intended for scientific production runs because it is not parallelized 
and uses a fixed-size 2D grid. On the other hand, the code performs fairly well because all array 
operations are implemented using numpy. As a result, many of the test problems run in less than a 
minute on a modern laptop. 

If you wish to cite Ulula, you can refer to the 
`ASCL entry <https://ascl.net/code/v/2875>`_. The algorithms used in the code are described in 
these `class notes (pdf) <http://www.benediktdiemer.com/wp-content/uploads/astr670_hydro_notes.pdf>`_. 
Please feel free to `get in touch <http://www.benediktdiemer.com/>`_ if you have any questions or 
if you would like to develop Ulula!

---------------------------------------------------------------------------------------------------
Contents
---------------------------------------------------------------------------------------------------

.. toctree::
    :maxdepth: 2

    installation
    run
    simulation
    plots
    setups
    versions

---------------------------------------------------------------------------------------------------
Search
---------------------------------------------------------------------------------------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
