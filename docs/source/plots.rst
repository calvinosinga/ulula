===================================================================================================
Plotting
===================================================================================================

This page documents Ulula's plotting capabilities.

---------------------------------------------------------------------------------------------------
Overview
---------------------------------------------------------------------------------------------------

Ulula comes with two plotting routines for 1D and 2D data, :func:`~ulula.plots.plot1d` and 
:func:`~ulula.plots.plot2d`. Both plot multiple panels for multiple fluid quantities that are 
given as a list of strings. Valid identifiers are listed :data:`~ulula.plots.fields` dictionary.
A simulation object must be passed to the plotting functions.

.. autosummary::
	~ulula.plots.fields
	~ulula.plots.getPlotQuantities
	~ulula.plots.plot1d
	~ulula.plots.plot2d

---------------------------------------------------------------------------------------------------
Plottable quantities
---------------------------------------------------------------------------------------------------

All plotting functions take a ``q_plot`` parameter that contains a list of the fluid variables to 
be plotted. Possible values include the primitive and conserved variables that the simulation 
keeps track of (see :doc:`simulation`), as well as some derived quantities:

============  ======
Abbreviation  Quantity
============  ======
``DN``        Density
``PR``        Pressure
``ET``        Total energy (thermal + kinetic + potentials)
``VX``        X-velocity
``VY``        Y-velocity
``VT``        Total velocity
``MX``        X-momentum
``MY``        Y-momentum
============  ======

The full list is contained in the following dictionary:

.. autodata:: ulula.plots.fields

.. autofunction:: ulula.plots.getPlotQuantities

---------------------------------------------------------------------------------------------------
Plotting functions
---------------------------------------------------------------------------------------------------

.. autofunction:: ulula.plots.plot1d

.. autofunction:: ulula.plots.plot2d
