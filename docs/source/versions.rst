===================================================================================================
Version history
===================================================================================================

.. rubric:: Version 0.2.2 (released 05/20/2021)

Minor changes:

* Added a check that the final time must be larger when restarting
* Added to documentation

.. rubric:: Version 0.2.1 (released 05/19/2021)

The main runtime interface (see :doc:`run`) was reworked significantly:

* it now accommodates saving and plotting at evenly spaced times rather than snapshots
* saving, plotting, and movie making can be switched on at the same time
* the output and plotting times do not interfere with the physical simulation

.. rubric:: Version 0.2.0 (released 05/17/2021)

The main features added in this version relate to input/output of files:

* Added file saving into hdf5 snapshot files
* Added restarting capability (either manual or in runtime function)
* Added dictionaries of primitive and conserved fluid quantities for more transparency in the plotting
  and saving routines
* Added to documentation

.. rubric:: Version 0.1.1 (released 04/02/2021)

This version is mostly a refactoring of the initial checkin, and also contains:

* Added Sedov-Taylor test case (blastwave explosion)
* Added radially averaged 1D plots of fluid quantities

.. rubric:: Version 0.1.0 (released 03/24/2021)

Initial version. Contains the main hydro solver and examples.
