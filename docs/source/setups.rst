===================================================================================================
Hydro problem setups
===================================================================================================

This page describes Ulula's general system for setting up hydro problems, as well as some specific 
setups that describe classical hydro problems. 

---------------------------------------------------------------------------------------------------
Overview
---------------------------------------------------------------------------------------------------

The :class:`~ulula.setup_base.Setup` class contains two methods that must be implemented by each
subclass: providing a name for the problem and setting initial conditions. All other routines can
optionally be overwritten. Some of those routines are passed to the Ulula plotting functions when
using the :func:`~ulula.run.run` function. In addition to the general superclass, the following 
specific setups are implemented:

.. autosummary::
	~ulula.setups.advection.SetupAdvect
	~ulula.setups.kelvin_helmholtz.SetupKelvinHelmholtz
	~ulula.setups.sedov_taylor.SetupSedov
	~ulula.setups.shocktube.SetupSod
	~ulula.setups.shocktube.SetupSodX
	~ulula.setups.shocktube.SetupSodY

---------------------------------------------------------------------------------------------------
General setup class
---------------------------------------------------------------------------------------------------

.. autoclass:: ulula.setup_base.Setup
    :members:

---------------------------------------------------------------------------------------------------
Setup: Advection tests
---------------------------------------------------------------------------------------------------

The following image shows the results of the advection test for four combinations of hydro solvers.
This test can be run with the function given in the runtime examples (see :doc:`run`).

.. image:: ../images/setup_advect.png
   :align: center
   :scale: 40 %

The initially spherical and perfectly sharp circle with higher density is advected through the 
box on a diagonal trajectory. When using no spatial reconstruction (top left), the scheme is
extremely diffusive (and 1st order in space and time). When using linear reconstruction with the 
MinMod limiter but still a 1st-order in time Euler integrator (top right), the results are better 
but still pretty diffusive. When using a less diffusive slope limiter (MC, bottom left), the scheme
is unstable and explodes. Using the MUSCL-Hancock 2nd-order scheme (bottom right) leads to a 
stable and much less diffusive solution.

.. autoclass:: ulula.setups.advection.SetupAdvect
    :members:
       
---------------------------------------------------------------------------------------------------
Setup: Kelvin-Helmholtz instability
---------------------------------------------------------------------------------------------------
    
The following images show the evolution of the standard Kelvin-Helmholtz setup implemented in the
class below.

.. image:: ../images/setup_kh.png
   :align: center
   :scale: 40 %

This test can be run in less than a minute (see, e.g., the example function in :doc:`run`).
    
.. autoclass:: ulula.setups.kelvin_helmholtz.SetupKelvinHelmholtz
    :members:

---------------------------------------------------------------------------------------------------
Setup: Sedov-Taylor explosion
---------------------------------------------------------------------------------------------------

The following image shows the Sedov-Taylor test when the blastwave is about to reach the edge of
the box:

.. image:: ../images/setup_sedov.png
   :align: center
   :scale: 60 %

Ulula can also plot the results binned in radial annuli:

.. image:: ../images/setup_sedov_1d.png
   :align: center
   :scale: 50 %

The analytical solution is part of the setup class below.

.. autoclass:: ulula.setups.sedov_taylor.SetupSedov
    :members:
   
---------------------------------------------------------------------------------------------------
Setup: Shocktube tests
---------------------------------------------------------------------------------------------------

The image below shows the result of the shocktube test for a domain with 100 cells:

.. image:: ../images/setup_sod.png
   :align: center
   :scale: 50 %

The shocktube setup is implemented as a general class and child versions that elongate the 2D 
domain in the x and y directions. Both must give the same result, which tests that the solver does
not unphysically prefer one direction.

.. autoclass:: ulula.setups.shocktube.SetupSod
    :members:
    
.. autoclass:: ulula.setups.shocktube.SetupSodX
    :members:
    
.. autoclass:: ulula.setups.shocktube.SetupSodY
    :members:
